//  For the year 2016 get the extra runs conceded per team.
import java.io.*;
import java.util.*;

class match {
    protected SortedMap runConcededPerTeams() throws Exception {
        BufferedReader matchDetails = new BufferedReader(new FileReader("./matches.csv"));
        BufferedReader deliveryDetails = new BufferedReader(new FileReader("./deliveries.csv"));
        String matchLine = "";
        String deliveryLine = "";
        SortedMap<String, Integer> result = new TreeMap<>();
        ArrayList<String> idList = new ArrayList<>();
        while ((matchLine = matchDetails.readLine()) != null) {
            String[] matchRecord = matchLine.split(",");
            if (matchRecord[1].contains("2016")) {
                idList.add(matchRecord[0]);
            }
        }
        while ((deliveryLine = deliveryDetails.readLine()) != null) {
            String[] deliveryRecord = deliveryLine.split(",");
            if (idList.contains(deliveryRecord[0])) {
                if (result.containsKey(deliveryRecord[3])) {
                    result.put(deliveryRecord[3], result.get(deliveryRecord[3]) + Integer.parseInt(deliveryRecord[16]));
                } else {
                    result.put(deliveryRecord[3], Integer.parseInt(deliveryRecord[16]));
                }
            }
        }
        return result;
    }
}

public class runConceded {
    public static void main(String[] args) throws Exception {
        match ipl = new match();
        System.out.println("For the year 2016 get the extra runs conceded per team: \n" + ipl.runConcededPerTeams());
        BufferedWriter file = new BufferedWriter(new FileWriter("./solution3.txt", false));
        file.write("For the year 2016 get the extra runs conceded per team: \n" + ipl.runConcededPerTeams().toString());
        file.close();
    }
}

/*

For the year 2016 get the extra runs conceded per team:
{Delhi Daredevils=106, Gujarat Lions=98, Kings XI Punjab=100, Kolkata Knight Riders=122, Mumbai Indians=102, Rising Pune Supergiants=108, Royal Challengers Bangalore=156, Sunrisers Hyderabad=107}

 */

