//Number of matches won of all teams over all the years of IPL.

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.SortedMap;
import java.util.TreeMap;

class matches {
    SortedMap matchesWonPerTeamInAllYears() throws Exception {
        BufferedReader matchData = new BufferedReader(new FileReader("./matches.csv"));
        SortedMap<String, Integer> result = new TreeMap<>();
        String line = "";
        Integer temp;
        while ((line = matchData.readLine()) != null) {
            String[] record = line.split(",");
            if(record[10].length()==0){
                continue;
            }
            else if(record[0].contains("id")) {
                continue;
            } else if (result.containsKey(record[10])) {
                result.put(record[10], result.get(record[10]) + 1);
            }  else {
                temp = 1;
                result.put(record[10], temp);
            }
        }
        return result;
    }
}

public class matchesWonPerTeam {
    public static void main(String[] args) throws Exception {
        matches ipl = new matches();
        System.out.println("Number of matches won of all teams over all the years of IPL.: \n"+ipl.matchesWonPerTeamInAllYears());
        BufferedWriter file= new BufferedWriter(new FileWriter("solution2.txt"));
        file.write("Number of matches won of all teams over all the years of IPL: \n"+ipl.matchesWonPerTeamInAllYears().toString());
        file.close();
    }
}
/*

Number of matches won of all teams over all the years of IPL.:
{Chennai Super Kings=79, Deccan Chargers=29, Delhi Daredevils=62, Gujarat Lions=13, Kings XI Punjab=70, Kochi Tuskers Kerala=6, Kolkata Knight Riders=77, Mumbai Indians=92, Pune Warriors=12, Rajasthan Royals=63, Rising Pune Supergiant=10, Rising Pune Supergiants=5, Royal Challengers Bangalore=73, Sunrisers Hyderabad=42}

 */
