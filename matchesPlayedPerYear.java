// Number of matches played per year of all the years in IPL.

import java.io.*;
import java.util.*;

class Matches {
    SortedMap matchesPlayedPerYearInAllYears() throws Exception {
        BufferedReader matchDetails = new BufferedReader(new FileReader("./matches.csv"));
        Integer i = 0;
        String line = "";
        SortedMap<String, Integer> result = new TreeMap<>();
        while ((line = matchDetails.readLine()) != null) {
            String[] record = line.split(",");
            if (record[1].contains("season")) {
                continue;
            } else if (result.containsKey(record[1])) {
                result.put(record[1], i += 1);
            } else {
                i = 1;
                result.put(record[1], i);
            }
        }
        return result;
    }
}

public class matchesPlayedPerYear{
    public static void main(String[] args) throws Exception {
        Matches ipl = new Matches();
        System.out.println("Number of matches played per year of all the years in IPL: \n" + ipl.matchesPlayedPerYearInAllYears());
        BufferedWriter file = new BufferedWriter(new FileWriter("./solution1.txt", false));
        file.write("Number of matches played per year of all the years in IPL:\n" + ipl.matchesPlayedPerYearInAllYears().toString());
        file.close();
    }
}

/*
Number of matches played per year of all the years in IPL:
{2008=58, 2009=57, 2010=60, 2011=73, 2012=74, 2013=76, 2014=60, 2015=59, 2016=60, 2017=59}
 */
