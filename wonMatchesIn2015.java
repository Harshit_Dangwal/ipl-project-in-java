// Matches won by Mumbai Indians in yest 2015.
import java.util.*;
import java.io.*;

class matchesWon {
    SortedMap mumbaiIndianWonMatchesIn2015() throws Exception {
        BufferedReader matchesData = new BufferedReader(new FileReader("./matches.csv"));
        SortedMap<String, Integer> result = new TreeMap<>();
        String line = "";
        while ((line = matchesData.readLine()) != null) {
            String[] matchRecord = line.split(",");
            if (matchRecord[10].contains("Mumbai Indians")&&matchRecord[1].contains("2015")) {
                if (result.containsKey(matchRecord[10])){
                    result.put(matchRecord[10], result.get(matchRecord[10]) + 1);
                } else {
                    Integer temp = 1;
                    result.put(matchRecord[10], temp);
                }
            }else{
                continue;
            }
        }
        return result;
    }
}

public class wonMatchesIn2015 {
    public static void main(String[] args) throws Exception {
        matchesWon ipl = new matchesWon();
        System.out.println("Mumbai Indians total matches won in 2015: \n"+ipl.mumbaiIndianWonMatchesIn2015());
        BufferedWriter file = new BufferedWriter(new FileWriter("./solution5.txt"));
        file.write("Mumbai Indians total matches won in 2015: \n"+ipl.mumbaiIndianWonMatchesIn2015().toString());
        file.close();
    }
}

/*
Mumbai Indians total matches won in 2015:
{Mumbai Indians=10}
 */
